use crate::todo::Todo;
use axum::{
    extract::{Path, State},
    http::StatusCode,
    Json,
};
use serde_json::{json, Value};
use std::{
    collections::{hash_map::Entry, HashMap},
    sync::Arc,
};
use tokio::sync::RwLock;
use uuid::Uuid;

pub async fn root() -> Json<Value> {
    Json(json!({
        "links": {
            "status": "/status",
            "todos": "/todos",
        }
    }))
}

// '/status'
#[tracing::instrument]
pub async fn status() -> Result<Json<Value>, StatusCode> {
    let service_name = std::env::var("CARGO_PKG_NAME").map_err(|_| StatusCode::NOT_FOUND)?;
    let service_version = std::env::var("CARGO_PKG_VERSION").map_err(|_| StatusCode::NOT_FOUND)?;
    let git_hash = std::env::var("GIT_DESCRIBE").map_err(|_| StatusCode::NOT_FOUND)?;
    Ok(Json(json!({
        "service.name": service_name,
        "service.version": format!("{service_version}-{git_hash}"),
    })))
}

// '/todos'
#[tracing::instrument(skip(database))]
pub async fn get_todos(
    State(database): State<Arc<RwLock<HashMap<Uuid, Todo>>>>,
) -> Json<Vec<Todo>> {
    let todos = database.read().await.values().cloned().collect();
    Json(todos)
}

#[tracing::instrument(skip(database))]
pub async fn post_todo(
    State(database): State<Arc<RwLock<HashMap<Uuid, Todo>>>>,
    Json(task): Json<String>,
) -> (StatusCode, Json<Todo>) {
    let todo = Todo::new(task);
    database.write().await.insert(todo.id(), todo.clone());
    (StatusCode::CREATED, Json(todo))
}

// '/todos/:id'
#[tracing::instrument(skip(database))]
pub async fn get_todo(
    Path(id): Path<Uuid>,
    State(database): State<Arc<RwLock<HashMap<Uuid, Todo>>>>,
) -> Result<Json<Todo>, StatusCode> {
    database
        .read()
        .await
        .get(&id)
        .cloned()
        .map(Json)
        .ok_or(StatusCode::NOT_FOUND)
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct TodoPut {
    task: String,
    #[serde(default)]
    done: bool,
}
#[tracing::instrument(skip(database, new_todo), fields(task = new_todo.task, done = new_todo.done))]
pub async fn put_todo(
    Path(id): Path<Uuid>,
    State(database): State<Arc<RwLock<HashMap<Uuid, Todo>>>>,
    Json(new_todo): Json<TodoPut>,
) -> (StatusCode, Json<Todo>) {
    match database.write().await.entry(id) {
        Entry::Occupied(mut entry) => {
            tracing::debug!("modifying existing todo entry");
            entry.get_mut().set_task(new_todo.task);
            entry.get_mut().set_done(new_todo.done);
            (StatusCode::NO_CONTENT, Json(entry.get().clone()))
        }
        Entry::Vacant(entry) => {
            tracing::debug!("creating a new todo entry");
            let mut todo = Todo::from_id(id, new_todo.task);
            todo.set_done(new_todo.done);
            entry.insert(todo.clone());
            (StatusCode::CREATED, Json(todo))
        }
    }
}

#[tracing::instrument(skip(database))]
pub async fn delete_todo(
    Path(id): Path<Uuid>,
    State(database): State<Arc<RwLock<HashMap<Uuid, Todo>>>>,
) -> Result<(StatusCode, Json<Todo>), StatusCode> {
    database
        .write()
        .await
        .remove(&id)
        .map(Json)
        .map(|json| (StatusCode::NO_CONTENT, json))
        .ok_or(StatusCode::NOT_FOUND)
}

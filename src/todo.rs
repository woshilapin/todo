use uuid::Uuid;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Todo {
    id: Uuid,
    task: String,
    done: bool,
}

#[tracing::instrument(level = "trace")]
fn censor_task(mut task: String) -> String {
    static F_WORDS: [&str; 2] = ["duck", "fork"];

    for pattern in F_WORDS {
        tracing::trace!("replacing all instances of '{pattern}'");
        task = task.replace(pattern, "****");
    }
    task
}

#[tracing::instrument(level = "trace")]
fn trim_task(mut task: String) -> String {
    if task.len() > 42 {
        tracing::debug!("task's length is too long, trimming it!");
        task.truncate(40);
        format!("{task}...")
    } else {
        task
    }
}

#[tracing::instrument(level = "trace")]
fn validate_task(task: String) -> String {
    trim_task(censor_task(task))
}

impl Todo {
    #[tracing::instrument(level = "debug")]
    pub fn new(task: String) -> Self {
        let task = validate_task(task);
        Self {
            id: Uuid::new_v4(),
            task,
            done: false,
        }
    }

    #[tracing::instrument(level = "debug")]
    pub fn from_id(id: Uuid, task: String) -> Self {
        let task = validate_task(task);
        Self {
            id,
            task,
            done: false,
        }
    }

    #[tracing::instrument(level = "trace", skip(self))]
    pub fn id(&self) -> Uuid {
        self.id
    }
    #[tracing::instrument(level = "trace", skip(self))]
    pub fn set_task(&mut self, task: String) {
        let task = validate_task(task);
        self.task = task;
    }
    #[tracing::instrument(level = "trace", skip(self))]
    pub fn set_done(&mut self, done: bool) {
        self.done = done;
    }
}

use std::env;

fn main() {
    let git = gix::open(env::current_dir().expect("failed to get the current folder"))
        .expect("current folder should be a git repository");
    let head_commit = git.head_commit().expect("failed to find a commit for HEAD");
    let head_name = head_commit
        .describe()
        .id_as_fallback(true)
        .try_format()
        .expect("failed to generate format's describe HEAD")
        .expect("failed to describe HEAD");
    println!("cargo:rustc-env=GIT_DESCRIBE={head_name}");
}

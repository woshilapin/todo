use axum::{routing::MethodRouter, Router};
use std::{collections::HashMap, sync::Arc};
use tokio::sync::RwLock;
use tracing::Level;

mod logger;
mod routing;
mod todo;

#[tokio::main]
async fn main() {
    logger::init();
    let database = Arc::new(RwLock::new(HashMap::new()));
    let todos_router = Router::new()
        .route(
            "/",
            MethodRouter::new()
                .get(routing::get_todos)
                .post(routing::post_todo),
        )
        .route(
            "/:id",
            MethodRouter::new()
                .get(routing::get_todo)
                .put(routing::put_todo)
                .delete(routing::delete_todo),
        )
        .with_state(database)
        .layer(
            tower_http::trace::TraceLayer::new_for_http()
                .make_span_with(tower_http::trace::DefaultMakeSpan::new().level(Level::INFO)),
        );
    let app = Router::new()
        .route("/", MethodRouter::new().get(routing::root))
        .route("/status", MethodRouter::new().get(routing::status))
        .nest("/todos", todos_router);
    let addr = "0.0.0.0:3000";
    tracing::info!("starting 'todo' service on '{addr}'");
    axum::Server::bind(&addr.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

use tracing_subscriber::{layer::SubscriberExt as _, util::SubscriberInitExt as _};

pub fn init() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| "info".into()),
        )
        .with(tracing_subscriber::fmt::layer().pretty())
        .with({
            let tracer = opentelemetry_otlp::new_pipeline()
                .tracing()
                .with_exporter(opentelemetry_otlp::new_exporter().tonic())
                .install_batch(opentelemetry::runtime::Tokio)
                .unwrap();
            tracing_opentelemetry::layer().with_tracer(tracer)
        })
        .init();
}

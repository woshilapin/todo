set dotenv-load
set shell := ["/bin/zsh", "-c"]

jaeger:
	-@docker run \
		--rm \
		--hostname="jaeger" \
		--publish 4317:4317 \
		--publish 4318:4318 \
		--publish 16686:16686 \
		jaegertracing/all-in-one:1.49

serve:
	cargo run --release

root:
	xh GET localhost:3000/

status:
	xh GET localhost:3000/status

todos:
	xh GET localhost:3000/todos

post task:
	xh POST localhost:3000/todos <<< '"{{task}}"'

get id:
	xh GET localhost:3000/todos/{{id}}

put id task done="false":
	xh PUT localhost:3000/todos/{{id}} <<< '{"task":"{{task}}","done":{{done}}}'

delete id:
	xh DELETE localhost:3000/todos/{{id}}
